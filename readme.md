# Lions MD410 2023 Oceans of Opportunity Convention PDF Creator

A [kppe](https://gitlab.com/kimvanwyk/kppe/) based Docker image to produce branded PDF documentation for the Lions MD410 2023 Convention from kppe or [pandoc](https://pandoc.org/) flavoured markdown text files.

## Usage

Most easily executed from the directory containing the markdown file. The resulting PDF will be written to the same directory.

```bash
docker run -v ${PWD}:/io registry.gitlab.com/md410-2023-convention/pdf-creator test.txt
```
